package edu.ntnu.stud.chaosgame.model.data;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for the Complex class.
 */
public class Matrix2x2Test {

  /**
   * Test the constructor of the Matrix2x2 class by creating a new
   * Matrix2x2 instance and asserting the values of the matrix elements.
   */
  @Test
  void testMatrix2x2Constructor() {
    Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
    assertEquals(1.0, matrix.getA00());
    assertEquals(2.0, matrix.getA01());
    assertEquals(3.0, matrix.getA10());
    assertEquals(4.0, matrix.getA11());
  }

  /**
   * Test the multiply method of the Matrix2x2 class by creating
   * a matrix and a vector and multiplying them together.
   */
  @Test
  void testMultiply() {
    Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
    Vector2D vector = new Vector2D(1.0, 2.0);
    Vector2D result = matrix.multiply(vector);
    assertEquals(5.0, result.getX0());
    assertEquals(11.0, result.getX1());
  }
}