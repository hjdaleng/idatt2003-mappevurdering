package edu.ntnu.stud.chaosgame.model.data;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test for the Complex class.
 */
public class ComplexTest {

  /**
   * Test the constructor of the Complex class by creating a new
   * Complex instance and asserting the values of the real and imaginary parts.
   */
  @Test
  void testComplexConstructor() {
    Complex complex = new Complex(1.0, 2.0);
    assertEquals(1.0, complex.getX0());
    assertEquals(2.0, complex.getX1());
  }

  /**
   * Tests whether the sqrt-method works as expected by
   * comparing the result to the expected value.
   */
  @Test
  void testSqrt() {
    Complex complex = new Complex(4.0, 0.0);
    Complex sqrtComplex = complex.sqrt();
    assertEquals(2.0, sqrtComplex.getX0());
    assertEquals(0.0, sqrtComplex.getX1());
  }
}