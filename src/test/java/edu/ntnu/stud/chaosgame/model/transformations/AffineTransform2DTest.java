package edu.ntnu.stud.chaosgame.model.transformations;

import edu.ntnu.stud.chaosgame.model.data.Matrix2x2;
import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.transformations.AffineTransform2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the AffineTransform2D class.

 */
class AffineTransform2DTest {

  /**
   * Matrix2x2 object for testing.
   */
  Matrix2x2 matrix2x2;

  /**
   * Vector2D object for testing.
   */
  Vector2D vector2D;

  /**
   * AffineTransform2D object for testing.
   */
  AffineTransform2D affineTransform2D;

  /**
   * Set up the test environment by defining the fields.
   */
  @BeforeEach
  void setUp() {
    matrix2x2 = new Matrix2x2(0.5,1,1,0.5);
    vector2D = new Vector2D(3,1);
    affineTransform2D = new AffineTransform2D(matrix2x2,vector2D);

  }

  /**
   * Nested class for testing the AffineTransform2D class.
   */
  @Nested
  @DisplayName("Positive Affine Transform Tests")
  class PositiveAffineTransform2DTest {
    @Test
    void transform() {
      Vector2D point = new Vector2D(1, 1);
      Vector2D expected = new Vector2D(4.5,2.5);
      Vector2D actual = affineTransform2D.transform(point);
      assertEquals(expected.getX0(), actual.getX0(), "The transform method should correctly transform the X0 component");
      assertEquals(expected.getX1(), actual.getX1(), "The transform method should correctly transform the X1 component");
    }

    /**
     * Test the getMatrix method.
     */
    @Test
    void getMatrix() {
      Matrix2x2 actual = affineTransform2D.getMatrix();
      assertEquals(matrix2x2, actual, "The getMatrix method should return the correct matrix");
    }

    /**
     * Test the getVector method.
     */
    @Test
    void getVector() {
      Vector2D actual = affineTransform2D.getVector();
      assertEquals(vector2D, actual, "The getVector method should return the correct vector");
    }
  }

    /**
     * Nested class for negative testing of the AffineTransform2D class.
     */
    @Nested
    @DisplayName("Negative Affine Transform Tests")
    class NegativeAffineTransform2DTests{

    /**
     * Test the constructor with null matrix input.
     */
    @Test
    void transformWithNullInput() {
      assertThrows(IllegalArgumentException.class, () -> affineTransform2D.transform(null),
      "The transform method should throw IllegalArgumentException when the input is null");
    }
  }
}
