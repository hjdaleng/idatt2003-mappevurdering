package edu.ntnu.stud.chaosgame.model.game;

import edu.ntnu.stud.chaosgame.model.data.Complex;
import edu.ntnu.stud.chaosgame.model.data.Matrix2x2;
import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.game.ChaosGameDescription;
import edu.ntnu.stud.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.stud.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.stud.chaosgame.model.transformations.Transform2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the ChaosGameDescription class.
 */
class ChaosGameDescriptionTest {

  /**
   * The minimum coordinates of the description.
   */
  private Vector2D minCoords;

  /**
   * The maximum coordinates of the description.
   */
  private Vector2D maxCoords;

  /**
   * The list of transformations of the description.
   */
  private List<Transform2D> transforms2D;

  /**
   * The description of the chaos game.
   */
  private ChaosGameDescription description;

  /**
   * The complex number in the case of a Julia transformation.
   */
  Complex complex;

  /**
   * The matrix of the description.
   */
  Matrix2x2 matrix2x2;

  /**
   * The vector of the description.
   */
  Vector2D vector2D;

  /**
   * Set up the test environment.
   */
  @BeforeEach
  void setUp() {
    minCoords = new Vector2D(0, 1);
    maxCoords = new Vector2D(1,0);
    complex = new Complex(0.3,0.6);
    matrix2x2 = new Matrix2x2(0.5, 1, 1, 0.5);
    vector2D = new Vector2D(3,1);
    transforms2D = new ArrayList<>();
    transforms2D.add(new AffineTransform2D(matrix2x2,vector2D));
    transforms2D.add(new JuliaTransform(complex,1));
    description = new ChaosGameDescription(minCoords, maxCoords, transforms2D);
  }

  /**
   * Test the getTransforms method.
   */
  @Test
  void getTransforms() {
    assertInstanceOf(AffineTransform2D.class, description.getTransforms().getFirst());
    assertInstanceOf(JuliaTransform.class, description.getTransforms().get(1));
  }

  /**
   * Test the getMinCoords method.
   */
  @Test
  void getMinCoords() {
    assertEquals(0,minCoords.getX0());
    assertEquals(1,minCoords.getX1());
  }

  /**
   * Test the getMaxCoords method.
   */
  @Test
  void getMaxCoords() {
    assertEquals(1,maxCoords.getX0());
    assertEquals(0,maxCoords.getX1());
  }

  /**
   * Test the constructor of the ChaosGameDescription class.
   */
  @Test
  void testConstructorValues(){
    assertEquals(minCoords,description.getMinCoords());
    assertEquals(maxCoords,description.getMaxCoords());
    assertEquals(transforms2D, description.getTransforms());
  }
}