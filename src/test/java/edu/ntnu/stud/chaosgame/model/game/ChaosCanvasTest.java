package edu.ntnu.stud.chaosgame.model.game;
import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.game.ChaosCanvas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test for the ChaosCanvas {@link ChaosCanvas}.
 */
public class ChaosCanvasTest {

  private ChaosCanvas canvas;

  /**
   * Instantiate a canvas before each test
   */
  @BeforeEach
  void setUp() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(1, 1);
    this.canvas = new ChaosCanvas(100, 100, minCoords, maxCoords);
  }

  /**
   * Test whether creating the canvas worked properly, and simultaneously test whether
   * the getters work as expected.
   */
  @Test
  void shouldCreateCanvas() {
    ChaosCanvas testCanvas = new ChaosCanvas(100, 100,
        new Vector2D(0,0), new Vector2D(1,1));

    Assertions.assertEquals(testCanvas.getSize()[0], 100);
    Assertions.assertEquals(testCanvas.getSize()[1], 100);
    Assertions.assertEquals(testCanvas.getMinCoords().getX0(), 0);
    Assertions.assertEquals(testCanvas.getMinCoords().getX1(), 0);
    Assertions.assertEquals(testCanvas.getMaxCoords().getX0(), 1);
    Assertions.assertEquals(testCanvas.getMaxCoords().getX1(), 1);
  }

  /**
   * Test whether an arbitrary point on the canvas is obtained correctly and hence
   * equal to 0.
   */
  @Test
  void shouldGetPixel() {
    Assertions.assertEquals(this.canvas.getPixel(new Vector2D(0.5, 0.5)), 0);
  }

  /**
   * Test whether putting a new pixel in an arbitrary location on the canvas works
   *  as expected.
   */
  @Test
  void shouldPutPixel() {
    Vector2D point = new Vector2D(0.2, 0.3);
    this.canvas.putPixel(point);

    // Test whether new point was added
    Assertions.assertEquals(this.canvas.getPixel(point), 1);

    // Ensure another, arbitrary point was not also added
    Assertions.assertEquals(this.canvas.getPixel(new Vector2D(0.1, 0.4)), 0);
  }

  /**
   * Tests whether putting pixels in the intensity array works as expected, incrementing
   * by one for each time the pixel is visited.
   */
  @Test
  void shouldPutPixelAndIntensity() {
    Vector2D point = new Vector2D(0.2, 0.3);
    this.canvas.putPixel(point);
    this.canvas.putPixel(point);

    // Test whether new point was added
    Assertions.assertEquals(this.canvas.getPixel(point), 1);
    Assertions.assertEquals(this.canvas.getIntensityPixel(point), 2);

    // Ensure another, arbitrary point was not also added
    Assertions.assertEquals(this.canvas.getPixel(new Vector2D(0.1, 0.4)), 0);
  }

  /**
   * Test whether clearing the canvas works as expected.
   */
  @Test
  void shouldClearCanvas() {

    // Put pixels throughout a part of the canvas.
    for (int i = 1; i < 101; i++) {
      this.canvas.putPixel(new Vector2D( 1.0 / i, 1.0 / i));

      // Check that the of the points where a pixel was added, are not equal to 0.
      Assertions.assertNotEquals(0, this.canvas.getPixel(new Vector2D(1.0 / i, 1.0 / i)));
    }

    // Clear the canvas
    this.canvas.clearCanvas();

    // Ensure that each point where a pixel was added, is now equal to 0.
    for (int i = 1; i < 101; i++) {
      Assertions.assertEquals(this.canvas.getPixel(new Vector2D(1.0 / i, 1.0 / i)), 0);
    }
  }

  /**
   * Test whether the intensity of a pixel is obtained correctly.
   */
  @Test
  void shouldGetIntensityPixel() {
    Vector2D point = new Vector2D(0.2, 0.3);
    this.canvas.putPixel(point);
    this.canvas.putPixel(point);

    // Test whether new point was added with correct intensity
    Assertions.assertEquals(this.canvas.getIntensityPixel(point), 2);

    // Ensure another, arbitrary point has zero intensity
    Assertions.assertEquals(this.canvas.getIntensityPixel(new Vector2D(0.1, 0.4)), 0);
  }


}
