package edu.ntnu.stud.chaosgame.model.data;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for the Vector2D class.
 */
public class Vector2DTest {

  /**
   * Test the constructor of the Vector2D class by creating a new
   * Vector2D instance and asserting the values of the x0 and x1 components.
   */
  @Test
  void testVector2DConstructor() {
    Vector2D vector = new Vector2D(1.0, 2.0);
    assertEquals(1.0, vector.getX0());
    assertEquals(2.0, vector.getX1());
  }

  /**
   * Test the add method of the Vector2D class by creating two vectors and adding them together.
   */
  @Test
  void testAdd() {
    Vector2D vector1 = new Vector2D(1.0, 2.0);
    Vector2D vector2 = new Vector2D(3.0, 4.0);
    Vector2D sum = vector1.add(vector2);
    assertEquals(4.0, sum.getX0());
    assertEquals(6.0, sum.getX1());
  }

  /**
   * Test the subtract method of the Vector2D class by creating
   * two vectors and subtracting one from the other.
   */
  @Test
  void testSubtract() {
    Vector2D vector1 = new Vector2D(1.0, 2.0);
    Vector2D vector2 = new Vector2D(3.0, 4.0);
    Vector2D difference = vector1.subtract(vector2);
    assertEquals(-2.0, difference.getX0());
    assertEquals(-2.0, difference.getX1());
  }
}