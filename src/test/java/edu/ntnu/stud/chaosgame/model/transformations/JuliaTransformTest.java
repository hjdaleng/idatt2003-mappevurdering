package edu.ntnu.stud.chaosgame.model.transformations;

import edu.ntnu.stud.chaosgame.model.data.Complex;
import edu.ntnu.stud.chaosgame.model.data.Matrix2x2;
import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.transformations.JuliaTransform;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
class JuliaTransformTest {

  /**
   * Complex object for testing.
   */
  Complex complex;

  /**
   * Vector2D object for testing.
   */
  Vector2D vector2D;

  /**
   * JuliaTransform object for testing.

   */
  JuliaTransform juliaTransform;

  /**
   * Set up the test environment by defining the fields.
   */
  @BeforeEach
  void setUp() {

    complex = new Complex(0.3,0.6);
    vector2D = new Vector2D(3,1);
    juliaTransform = new JuliaTransform(complex,1);

  }

  /**
   * Nested class for testing the JuliaTransform class.
   */
  @Nested
  @DisplayName("Positive Tests for Julia Transformations")
  class JuliaTransformPositiveTests{

    /**
     * Test the getComplex method by asserting that the method returns the correct complex number.
     */
    @Test
    void getC1(){
      Complex actual = juliaTransform.getC1();
      assertEquals(complex,actual,"The getC1 should return the correct complex number");

    }

    /**
     * Test the getIterations method by asserting that the method
     * returns the correct number of iterations.
     */
    @Test
    void transform(){
      Vector2D expected = new Vector2D(1.647,0.12138);
      Vector2D actual = juliaTransform.transform(vector2D);
      assertEquals(expected.getX0(),actual.getX0(),0.01);
      assertEquals(expected.getX1(),actual.getX1(),0.01);

    }
  }

  /**
   * Nested class for testing the JuliaTransform class.
   */
  @Nested
  @DisplayName("Negative Tests for Julia Transformations")
  class JuliaTransformNegativeTests{

    /**
     * Test the getComplex method by asserting that the method returns the correct complex number.
     */
    @Test
    void transformZeroSign(){
       JuliaTransform juliaTransformZeroSign = new JuliaTransform(complex,0);
       assertEquals(vector2D.getX0(),juliaTransformZeroSign.transform(vector2D).getX0());
       assertEquals(vector2D.getX1(),juliaTransformZeroSign.transform(vector2D).getX1());
    }
  }
}