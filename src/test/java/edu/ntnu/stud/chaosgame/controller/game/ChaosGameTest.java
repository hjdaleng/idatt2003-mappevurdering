package edu.ntnu.stud.chaosgame.controller.game;

import edu.ntnu.stud.chaosgame.model.game.ChaosCanvas;
import edu.ntnu.stud.chaosgame.controller.game.ChaosGame;
import edu.ntnu.stud.chaosgame.model.game.ChaosGameDescription;
import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.generators.ChaosGameDescriptionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test for ChaosGame {@link ChaosGame}
 */
public class ChaosGameTest {

  /**
   * The ChaosGame object for testing.
   */
  private ChaosGame game;

  /**
   * Set up the test environment by defining the fields.
   */
  @BeforeEach
  void setUp() {
    ChaosGameDescriptionFactory factory = new ChaosGameDescriptionFactory();
    ChaosGameDescription description = factory.getDescriptions().get(0);
    ChaosCanvas canvas = new ChaosCanvas(100, 100, description.getMinCoords(), description.getMaxCoords());
    this.game = new ChaosGame(description, canvas);

  }

  /**
   * Test whether creating the ChaosGame works as expected.
   */
  @Test
  void shouldCreateChaosGame() {
    ChaosGameDescriptionFactory factory = new ChaosGameDescriptionFactory();
    ChaosGameDescription description = factory.getDescriptions().get(0);
    ChaosCanvas canvas = new ChaosCanvas(100, 100, description.getMinCoords(), description.getMaxCoords());
    this.game = new ChaosGame(description, canvas);

    Assertions.assertEquals(100, this.game.getCanvas().getWidth());
    Assertions.assertEquals(100, this.game.getCanvas().getHeight());
    Assertions.assertEquals(description.getMinCoords(), this.game.getCanvas().getMinCoords());
    Assertions.assertEquals(description.getMaxCoords(), this.game.getCanvas().getMaxCoords());
  }

  /**
   * Test whether running steps successfully changes the current point.
   */
  @Test
  void shouldRunStepsAndChangeCurrentPoint() {
    Vector2D pointOne = this.game.getCurrentPoint();
    this.game.runSteps(10);
    Vector2D pointTwo = this.game.getCurrentPoint();

    // Ensure starting point is not the same as the point after running steps
    Assertions.assertNotEquals(pointOne, pointTwo);
  }

}
