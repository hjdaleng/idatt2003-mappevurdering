package edu.ntnu.stud.chaosgame.controller.utility;

import edu.ntnu.stud.chaosgame.model.data.Complex;
import edu.ntnu.stud.chaosgame.model.data.Matrix2x2;
import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.game.ChaosGameDescription;
import edu.ntnu.stud.chaosgame.controller.utility.ChaosGameFileHandler;
import edu.ntnu.stud.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.stud.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.stud.chaosgame.model.transformations.Transform2D;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Test class for the ChaosGameFileHandler class.
 */
class ChaosGameFileHandlerTest {

  /**
   * Test reading from a file with Affine2D transforms.
   *
   * @throws IOException if an I/O error occurs.
   */
  @Test
  void testReadFromFileAffine2D() throws IOException {
    String fileContent = """
      Affine2D# Comment in line
      TestName
      0.0, 0.0 # Comment 
      1.0, 1.0 # Comment 
      0.5, 1.0, 1.0, 0.5, 3.0, 1.0 # Comment 
      0.5, 1.0, 1.0, 0.5, 3.0, 1.0 # Comment 
      0.5, 1.0, 1.0, 0.5, 3.0, 1.0 # Comment 
      """;

    Path tempFile = Files.createTempFile("test", ".txt");
    Files.writeString(tempFile, fileContent);
    ChaosGameFileHandler fileHandler = new ChaosGameFileHandler();
    ChaosGameDescription description = fileHandler.readFromFile(tempFile.toString());

    Vector2D minCoords = description.getMinCoords();
    Vector2D maxCoords = description.getMaxCoords();

    assertEquals(0.0, minCoords.getX0());
    assertEquals(0.0, minCoords.getX1());
    assertEquals(1.0, maxCoords.getX0());
    assertEquals(1.0, maxCoords.getX1());

    for (var transform : description.getTransforms()) {
      assertEquals(AffineTransform2D.class, transform.getClass());
      AffineTransform2D affine = (AffineTransform2D) transform;
      Matrix2x2 matrix = affine.getMatrix();
      Vector2D vector = affine.getVector();

      assertEquals(0.5, matrix.getA00());
      assertEquals(1.0, matrix.getA01());
      assertEquals(1.0, matrix.getA10());
      assertEquals(0.5, matrix.getA11());
      assertEquals(3.0, vector.getX0());
      assertEquals(1.0, vector.getX1());
    }
  }

  /**
   * Test reading from a file with Julia transforms.
   *
   * @throws IOException if an I/O error occurs.
   */
  @Test
  void testReadFromFileJulia() throws IOException {
    String fileContent = """
      Julia # Comment \n
      TestName 
      -1.6, -1 # Comment \n
      1.6, 1 # Comment \n
      -.74543, .11301 # Comment
      """;

    Path tempFile = Files.createTempFile("test", ".txt");
    Files.writeString(tempFile, fileContent);
    ChaosGameFileHandler fileHandler = new ChaosGameFileHandler();
    ChaosGameDescription description = fileHandler.readFromFile(tempFile.toString());

    Vector2D minCoords = description.getMinCoords();
    Vector2D maxCoords = description.getMaxCoords();

    assertEquals(-1.6, minCoords.getX0());
    assertEquals(-1, minCoords.getX1());
    assertEquals(1.6, maxCoords.getX0());
    assertEquals(1, maxCoords.getX1());

    for (var transform : description.getTransforms()) {
      assertEquals(JuliaTransform.class, transform.getClass());
      JuliaTransform julia = (JuliaTransform) transform;
      Complex complex = julia.getC1();


      assertEquals(-.74543, complex.getX0());
      assertEquals(.11301, complex.getX1());

    }
  }

  /**
   * Test writing to a file with Affine2D transforms.
   *
   * @throws IOException if an I/O error occurs.
   */
  @Test
  void testAffine2DWriteToFile() throws IOException {
    // Create a ChaosGameDescription with three transforms
    ChaosGameDescription description = getChaosGameDescription();

    // Write to a temporary file
    Path tempFile = Files.createTempFile("test", ".txt");
    ChaosGameFileHandler fileHandler = new ChaosGameFileHandler();
    fileHandler.writeToFile(description, tempFile.toString());

    // Read the file and verify its contents
    List<String> lines = Files.readAllLines(tempFile);
    assertEquals("Affine2D", lines.get(0));
    assertEquals("testName", lines.get(1));
    assertEquals("0.0, 0.0", lines.get(2));
    assertEquals("1.0, 1.0", lines.get(3));
    assertEquals("0.5, 1.0, 1.0, 0.5, 1.0, 1.0", lines.get(4));
    assertEquals("0.5, 0.0, 0.0, 0.5, 0.0, 0.0", lines.get(5));
    assertEquals("1.0, 0.2, 0.2, 1.0, 0.5, 0.5", lines.get(6));
  }

  /**
   * Get a ChaosGameDescription with three Affine2D transforms.
   *
   * @return a ChaosGameDescription with three Affine2D transforms.
   */
  private static ChaosGameDescription getChaosGameDescription() {
    Vector2D minCoords = new Vector2D(0.0, 0.0);
    Vector2D maxCoords = new Vector2D(1.0, 1.0);
    String name = "testName";
    Matrix2x2 matrix1 = new Matrix2x2(0.5, 1.0, 1.0, 0.5);
    Matrix2x2 matrix2 = new Matrix2x2(0.5, 0.0, 0.0, 0.5);
    Matrix2x2 matrix3 = new Matrix2x2(1.0, 0.2, 0.2, 1.0);
    List<Transform2D> transforms = getTransform2DS(matrix1, matrix2, matrix3);
    return new ChaosGameDescription(minCoords, maxCoords, transforms, name);
  }

  /**
   * Get a list of three Affine2D transforms.
   *
   * @param matrix1 the first matrix.
   * @param matrix2 the second matrix.
   * @param matrix3 the third matrix.
   * @return a list of three Affine2D transforms.
   */
  private static List<Transform2D> getTransform2DS(Matrix2x2 matrix1,
      Matrix2x2 matrix2, Matrix2x2 matrix3) {
    Vector2D vector1 = new Vector2D(1.0, 1.0);
    Vector2D vector2 = new Vector2D(0.0, 0.0);
    Vector2D vector3 = new Vector2D(0.5, 0.5);
    Transform2D transform1 = new AffineTransform2D(matrix1, vector1);
    Transform2D transform2 = new AffineTransform2D(matrix2, vector2);
    Transform2D transform3 = new AffineTransform2D(matrix3, vector3);
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(transform1);
    transforms.add(transform2);
    transforms.add(transform3);
    return transforms;
  }

  /**
   * Test writing to a file with Julia transforms.
   *
   * @throws IOException if an I/O error occurs.
   */
  @Test
  void testJuliaWriteToFile() throws IOException {
    // Create a ChaosGameDescription
    Vector2D minCoords = new Vector2D(0.0, 0.0);
    Vector2D maxCoords = new Vector2D(1.0, 1.0);
    Complex complex = new Complex(0.5,0.5);
    String name = "testName2";
    Transform2D transform = new JuliaTransform(complex, 1);
    List<Transform2D> transforms = Collections.singletonList(transform);
    ChaosGameDescription description = new ChaosGameDescription(minCoords, maxCoords, transforms, name);

    // Write to a temporary file
    Path tempFile = Files.createTempFile("test", ".txt");
    ChaosGameFileHandler fileHandler = new ChaosGameFileHandler();
    fileHandler.writeToFile(description, tempFile.toString());

    // Read the file and verify its contents
    List<String> lines = Files.readAllLines(tempFile);
    assertEquals("Julia", lines.get(0));
      assertEquals("testName2", lines.get(1));
    assertEquals("0.0, 0.0", lines.get(2));
    assertEquals("1.0, 1.0", lines.get(3));
    assertEquals("0.5, 0.5", lines.get(4));
  }

}

