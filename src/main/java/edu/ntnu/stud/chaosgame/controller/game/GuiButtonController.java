package edu.ntnu.stud.chaosgame.controller.game;

import edu.ntnu.stud.chaosgame.controller.utility.ChaosGameFileHandler;
import edu.ntnu.stud.chaosgame.controller.utility.PopupManager;
import edu.ntnu.stud.chaosgame.model.game.ChaosCanvas;
import edu.ntnu.stud.chaosgame.model.game.ChaosGameDescription;
import edu.ntnu.stud.chaosgame.model.generators.ChaosGameDescriptionFactory;
import edu.ntnu.stud.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.stud.chaosgame.view.ChaosCanvasToImageConverter;
import edu.ntnu.stud.chaosgame.view.ChaosGameGui;
import edu.ntnu.stud.chaosgame.view.GuiButtonObserver;
import edu.ntnu.stud.chaosgame.view.modificationpopups.Affine2DPopup;
import edu.ntnu.stud.chaosgame.view.modificationpopups.JuliaPopup;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ComboBox;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.imageio.ImageIO;

/**
 * Controller class for handling GUI button actions.
 * It notifies registered observers when a button is pressed.
 */
public class GuiButtonController {

  /**
   * The GUI this controller is associated with.
   */
  private final ChaosGameGui gui;

  /**
   * The timeline for the game.
   */
  private final Timeline timeline;

  /**
   * The controller for the popup buttons.
   */
  private final PopupButtonController popupButtonController;

  /**
   * The list of observers.
   */
  private final List<GuiButtonObserver> observers = new ArrayList<>();

  /**
   * The file handler for the chaos game.
   */
  private final ChaosGameFileHandler fileHandler;

  /**
   * The chaos game.
   */
  private ChaosGame game;

  /**
   * The factory for the chaos game descriptions.
   */
  private ChaosGameDescriptionFactory factory;

  /**
   * The canvas for the chaos game.
   */
  private ChaosCanvas chaosCanvas;

  /**
   * The current image path.
   */
  private final String currentImagePath = System.getProperty("user.home");

  /**
   * The step counter for the game.
   */
  private int stepCounter = 0;

  /**
   * The reusable image for the canvas.
   */
  private final WritableImage reusableImage;

  /**
   * Constructs a GuiButtonController with the given game and GUI.
   *
   * @param game the Chaos Game model.
   * @param gui the Chaos Game GUI.
   */
  public GuiButtonController(ChaosGame game, ChaosGameGui gui) {
    this.game = game;
    this.gui = gui;
    this.timeline = new Timeline(new KeyFrame(Duration.seconds(0.05),
        event -> this.drawChaosGame()));
    this.timeline.setCycleCount(Timeline.INDEFINITE);
    this.factory = new ChaosGameDescriptionFactory();
    this.chaosCanvas = new ChaosCanvas(1000, 1000,
        this.factory.getDescriptions().get(0).getMinCoords(),
        this.factory.getDescriptions().get(0).getMaxCoords());
    this.fileHandler = new ChaosGameFileHandler();
    this.popupButtonController = new PopupButtonController();
    this.reusableImage = new WritableImage(chaosCanvas.getWidth(), chaosCanvas.getHeight());

    addObserver(gui);

    gui.getStartButton().setOnAction(event -> notifyStartButtonPressed());
    gui.getStopButton().setOnAction(event -> notifyStopButtonPressed());
    gui.getClearButton().setOnAction(event -> notifyClearButtonPressed());
    gui.getQuitButton().setOnAction(event -> notifyQuitButtonPressed());
    gui.getSaveImageButton().setOnAction(event -> notifySaveImageButtonPressed());
    gui.getLoadFractalFromFileButton().setOnAction(event ->
        notifyLoadFractalFromFileButtonPressed());
    gui.getWriteToFileButton().setOnAction(event -> notifyWriteToFileButtonPressed());
    gui.getModifyGameButton().setOnAction(event -> notifyModifyGameButtonPressed());

    initializeDescriptionComboBox();
  }

  /**
   * Adds an observer to the list of observers.
   *
   * @param observer the observer to add.
   */
  public void addObserver(GuiButtonObserver observer) {
    observers.add(observer);
  }

  /**
   * Notifies all observers that the start button has been pressed.
   */
  private void notifyStartButtonPressed() {
    for (GuiButtonObserver observer : observers) {
      observer.onStartButtonPressed();
    }
  }

  /**
   * Notifies all observers that the stop button has been pressed.
   */
  private void notifyStopButtonPressed() {
    for (GuiButtonObserver observer : observers) {
      observer.onStopButtonPressed();
    }
  }

  /**
   * Notifies all observers that the clear button has been pressed.
   */
  private void notifyClearButtonPressed() {
    for (GuiButtonObserver observer : observers) {
      observer.onClearButtonPressed();
    }
  }

  /**
   * Notifies all observers that the quit button has been pressed.
   */
  private void notifyQuitButtonPressed() {
    for (GuiButtonObserver observer : observers) {
      observer.onQuitButtonPressed();
    }
  }

  /**
   * Notifies all observers that the save image button has been pressed.
   */
  private void notifySaveImageButtonPressed() {
    for (GuiButtonObserver observer : observers) {
      observer.onSaveImageButtonPressed();
    }
  }

  /**
   * Notifies all observers that the load fractal from file button has been pressed.
   */
  private void notifyLoadFractalFromFileButtonPressed() {
    for (GuiButtonObserver observer : observers) {
      observer.onLoadFractalFromFileButtonPressed();
    }
  }

  /**
   * Notifies all observers that the write to file button has been pressed.
   */
  private void notifyWriteToFileButtonPressed() {
    for (GuiButtonObserver observer : observers) {
      observer.onWriteToFileButtonPressed();
    }
  }

  /**
   * Notifies all observers that the modify game button has been pressed.
   */
  private void notifyModifyGameButtonPressed() {
    for (GuiButtonObserver observer : observers) {
      observer.onModifyGameButtonPressed();
    }
  }

  /**
   * Starts the game.
   */
  public void startGame() {
    timeline.play();
    gui.getStartButton().setText("Start");
  }

  /**
   * Stops the game.
   */
  public void stopGame() {
    timeline.stop();
    gui.getStartButton().setText("Resume");
  }

  /**
   * Clears the canvas.
   */
  public void clearCanvas() {
    chaosCanvas.clearCanvas();
    GraphicsContext gc = gui.getCanvas().getGraphicsContext2D();
    gc.clearRect(0, 0, gui.getCanvas().getWidth(), gui.getCanvas().getHeight());
    gui.getImageView().setImage(null);
    stepCounter = 0;
  }

  /**
   * Quits the game.
   */
  public void quitGame() {
    Platform.exit();
  }

  /**
   * Saves the image.
   */
  public void saveImage() {

    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Save Image");
    fileChooser.setInitialDirectory(new File(currentImagePath));
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
        "PNG Files", "*.png"));
    File file = fileChooser.showSaveDialog(gui.getStage());
    WritableImage image = gui.getImageView().snapshot(null, null);

    if (file != null) {
      try {
        BufferedImage bufferedImage = toBufferedImage(image);
        ImageIO.write(bufferedImage, "png", file);
      } catch (IOException e) {
        PopupManager.displayError("Save error",
            "There was an error whilst saving the image.");
      }
    }
  }

  /**
   * Converts a WritableImage to a BufferedImage.
   *
   * @param image the WritableImage to convert.
   * @return the converted BufferedImage.
   */
  private BufferedImage toBufferedImage(WritableImage image) {
    int width = (int) image.getWidth();
    int height = (int) image.getHeight();
    BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    PixelReader reader = image.getPixelReader();
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        Color color = reader.getColor(x, y);
        int r = (int) (color.getRed() * 255);
        int g = (int) (color.getGreen() * 255);
        int b = (int) (color.getBlue() * 255);
        int a = (int) (color.getOpacity() * 255);
        int argb = (a << 24) | (r << 16) | (g << 8) | b;
        bufferedImage.setRGB(x, y, argb);
      }
    }
    return bufferedImage;
  }

  /**
   * Loads a fractal from a file.
   */
  public void loadFractalFromFile() {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Fractal File");
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
        "Text Files", "*.txt"));
    File selectedFile = fileChooser.showOpenDialog(gui.getStage());
    if (selectedFile != null) {
      Path dest = Paths.get(
          "src/main/resources/descriptions/saved_descriptions/" + selectedFile.getName());
      if (Files.exists(dest)) {
        PopupManager.displayError("Error", "File already exists in the target location.");
      } else {
        try {
          Files.copy(selectedFile.toPath(), dest);
          factory = new ChaosGameDescriptionFactory();
        } catch (IOException e) {
          PopupManager.displayError("Error", "Could not load the fractal from file.");
        }
      }
    }
  }

  /**
   * Writes the fractal to a file.
   */
  public void writeFractalToFile() {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Save File");
    fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
    fileChooser.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("Text Files", "*.txt"),
        new FileChooser.ExtensionFilter("All Files", "*.*")
    );
    File file = fileChooser.showSaveDialog(gui.getStage());

    if (file != null) {
      try {
        ChaosGameDescription description = game.getDescription();
        fileHandler.writeToFile(description, file.getPath());
      } catch (IOException e) {
        PopupManager.displayError("Error", "Could not write the ChaosGameDescription to file.");
      }
    }
  }

  /**
   * Modifies the game based on the selected transformation.
   */
  public void modifyGame() {
    ChaosGameDescription description = game.getDescription();
    if (description.getTransforms().get(0) instanceof JuliaTransform) {
      JuliaPopup juliaPopup = new JuliaPopup(description);
      juliaPopup.getPopupModifyStage().setUserData(this);
      popupButtonController.handleUpdateButton(juliaPopup);
      juliaPopup.display();
    } else {
      Affine2DPopup affine2dPopup = new Affine2DPopup(description);
      affine2dPopup.getPopupModifyStage().setUserData(this);
      popupButtonController.handleUpdateButton(affine2dPopup);
      affine2dPopup.display();
    }
  }

  /**
   * Initializes the description combo box.
   */
  private void initializeDescriptionComboBox() {
    ComboBox<String> descriptionComboBox = gui.getDescriptionComboBox();
    descriptionComboBox.setOnShowing(event -> updateDescriptionComboBox());

    descriptionComboBox.getSelectionModel().selectedItemProperty()
            .addListener((observable, oldValue, newValue) -> {
              for (int i = 0; i < factory.getDescriptions().size(); i++) {
                ChaosGameDescription description = factory.getDescriptions().get(i);
                if (description.getName().equals(newValue)) {
                  updateDescription(i);
                  break;
                }
              }
            });
  }

  /**
   * Update the description of the chaos game..
   */
  public void updateDescriptionComboBox() {
    factory = new ChaosGameDescriptionFactory();
    ComboBox<String> descriptionComboBox = gui.getDescriptionComboBox();
    descriptionComboBox.getItems().clear();
    for (ChaosGameDescription description : factory.getDescriptions()) {
      descriptionComboBox.getItems().add(description.getName());
    }
  }

  /**
   * Draws the chaos game on the canvas.
   */
  public void drawChaosGame() {
    int iterationLimit = Integer.parseInt(gui.getIterationLimitTextField().getText());

    if (stepCounter >= iterationLimit) {
      timeline.stop();
      return;
    }

    // Run the number of steps indicated in the text field, else 1000.
    game.runSteps(!Objects.equals(gui.getStepCountTextField().getText(), "")
        ? Integer.parseInt(gui.getStepCountTextField().getText()) : 1000);

    ChaosCanvasToImageConverter converter = new ChaosCanvasToImageConverter(chaosCanvas,
        gui.getColorCheckBox().isSelected(), reusableImage);

    WritableImage image = converter.getImage();
    gui.getCanvas().getGraphicsContext2D().drawImage(image, 0, 0);
    gui.getImageView().setImage(image);
    stepCounter++;
  }

  /**
   * Updates the description of the chaos game.
   *
   * @param index the index of the new description in the list of factory descriptions.
   */
  public void updateDescription(int index) {
    timeline.stop();
    chaosCanvas.clearCanvas();
    gui.getCanvas().getGraphicsContext2D().clearRect(0, 0, gui.getCanvas()
            .getGraphicsContext2D().getCanvas().getWidth(),
        gui.getCanvas().getGraphicsContext2D().getCanvas().getHeight());
    clearCanvas();
    chaosCanvas.clearCanvas();

    ChaosGameDescription description = factory.getDescriptions().get(index);
    chaosCanvas = new ChaosCanvas(1000, 1000, description.getMinCoords(),
        description.getMaxCoords());

    gui.updateCanvas(chaosCanvas);

    game = new ChaosGame(description, chaosCanvas);

    stepCounter = 0;
  }
}

