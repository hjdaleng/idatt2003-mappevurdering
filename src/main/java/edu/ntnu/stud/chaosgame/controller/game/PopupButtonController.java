package edu.ntnu.stud.chaosgame.controller.game;

import edu.ntnu.stud.chaosgame.controller.utility.ChaosGameFileHandler;
import edu.ntnu.stud.chaosgame.controller.utility.PopupManager;
import edu.ntnu.stud.chaosgame.model.data.Complex;
import edu.ntnu.stud.chaosgame.model.data.Matrix2x2;
import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.game.ChaosGameDescription;
import edu.ntnu.stud.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.stud.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.stud.chaosgame.model.transformations.Transform2D;
import edu.ntnu.stud.chaosgame.view.PopupObserver;
import edu.ntnu.stud.chaosgame.view.modificationpopups.AbstractPopup;
import edu.ntnu.stud.chaosgame.view.modificationpopups.Affine2DPopup;
import edu.ntnu.stud.chaosgame.view.modificationpopups.JuliaPopup;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 * Controller class for handling the buttons related to modifying the chaos
 * game in the user interface.
 */
public class PopupButtonController {

  /**
   * The file handler for handling file operations.
   */
  private final ChaosGameFileHandler fileHandler;

  /**
   * The observer for the PopupButtonController.
   */
  private PopupObserver observer;

  /**
   * Constructor for PopupButtonController.
   */
  public PopupButtonController() {
    this.fileHandler = new ChaosGameFileHandler();
  }

  /**
   * Handles the update button action for the Affine2DPopup.
   *
   * @param popup the Affine2DPopup to handle the button for.
   */
  public void handleUpdateButton(Affine2DPopup popup) {
    popup.getUpdateButton().setOnAction(event -> {
      ChaosGameDescription newDescription = createChaosGameDescriptionFromAffine2dPopup(popup);
      saveAndNotify(popup, newDescription);
    });
  }

  /**
   * Handles the update button action for the JuliaPopup.
   *
   * @param popup the JuliaPopup to handle the button for.
   */
  public void handleUpdateButton(JuliaPopup popup) {
    popup.getUpdateButton().setOnAction(event -> {
      ChaosGameDescription newDescription = createChaosGameDescriptionFromJuliaPopup(popup);
      saveAndNotify(popup, newDescription);
    });
  }

  /**
   * Creates a ChaosGameDescription from the data in an Affine2DPopup.
   *
   * @param popup the Affine2DPopup to extract data from.
   * @return the created ChaosGameDescription.
   */
  private ChaosGameDescription createChaosGameDescriptionFromAffine2dPopup(Affine2DPopup popup) {
    String name = popup.getNameTextField().getText();
    Vector2D minCoords = new Vector2D(
        Double.parseDouble(popup.getMinXTextField().getText()),
        Double.parseDouble(popup.getMinYTextField().getText())
    );
    Vector2D maxCoords = new Vector2D(
        Double.parseDouble(popup.getMaxXTextField().getText()),
        Double.parseDouble(popup.getMaxYTextField().getText())
    );

    List<Transform2D> transforms = new ArrayList<>();
    for (int i = 0; i < popup.getMatrixTextFields().length; i++) {
      double a00 = Double.parseDouble(popup.getMatrixTextFields()[i][0].getText());
      double a01 = Double.parseDouble(popup.getMatrixTextFields()[i][1].getText());
      double a10 = Double.parseDouble(popup.getMatrixTextFields()[i][2].getText());
      double a11 = Double.parseDouble(popup.getMatrixTextFields()[i][3].getText());
      double b0 = Double.parseDouble(popup.getVectorTextFields()[2 * i].getText());
      double b1 = Double.parseDouble(popup.getVectorTextFields()[2 * i + 1].getText());

      transforms.add(new AffineTransform2D(new Matrix2x2(a00, a01, a10, a11),
          new Vector2D(b0, b1)));
    }

    return new ChaosGameDescription(minCoords, maxCoords, transforms, name);
  }

  /**
   * Creates a ChaosGameDescription from the data in a JuliaPopup.
   *
   * @param popup the JuliaPopup to extract data from.
   * @return the created ChaosGameDescription.
   */
  private ChaosGameDescription createChaosGameDescriptionFromJuliaPopup(JuliaPopup popup) {
    String name = popup.getNameTextField().getText();
    Vector2D minCoords = new Vector2D(
        Double.parseDouble(popup.getMinXTextField().getText()),
        Double.parseDouble(popup.getMinYTextField().getText())
    );
    Vector2D maxCoords = new Vector2D(
        Double.parseDouble(popup.getMaxXTextField().getText()),
        Double.parseDouble(popup.getMaxYTextField().getText())
    );

    double realPart = Double.parseDouble(popup.getRealPartTextField().getText());
    double imagPart = Double.parseDouble(popup.getImagPartTextField().getText());
    JuliaTransform transform = new JuliaTransform(new Complex(realPart, imagPart), 1);

    return new ChaosGameDescription(minCoords, maxCoords, List.of(transform), name);
  }

  /**
   * Saves the ChaosGameDescription to a file and shows a confirmation popup.
   *
   * @param popup the popup to show the confirmation for.
   * @param newDescription the ChaosGameDescription to save.
   */
  private void saveAndNotify(AbstractPopup popup, ChaosGameDescription newDescription) {
    try {
      String filePath = Paths.get("src/main/resources/descriptions/saved_descriptions",
          newDescription.getName() + ".txt").toString();
      fileHandler.writeToFile(newDescription, filePath);
      showConfirmationPopup(popup);
      if (observer != null) {
        observer.onUpdate(newDescription);
      }
    } catch (IOException e) {
      PopupManager.displayError("Error",
          "Could not save the ChaosGameDescription to file.");
    }
  }

  /**
   * Shows a confirmation popup when the ChaosGameDescription has been updated and saved.
   *
   * @param popup the popup to show the confirmation for.
   */
  private void showConfirmationPopup(AbstractPopup popup) {
    Alert alert = new Alert(Alert.AlertType.INFORMATION,
        "Updated ChaosGameDescription has been saved.", ButtonType.OK);
    alert.initOwner(popup.getPopupModifyStage());
    alert.showAndWait().ifPresent(response -> {
      if (response == ButtonType.OK) {
        popup.getPopupModifyStage().close();
      }
    });
  }
}
