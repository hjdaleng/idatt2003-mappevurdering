package edu.ntnu.stud.chaosgame.controller.utility;

import java.util.function.UnaryOperator;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;

/**
 * This class sets formatting constraints for certain UI components.
 */
public class Formatter {


  /**
   * A formatter for text fields that only allows for floating point numbers.
   */
  public static UnaryOperator<Change> floatFormatter = change -> {
    String newText = change.getControlNewText();
    if (newText.matches("-?([0-9]*[.])?[0-9]*")) {
      return change;
    }
    return null;
  };

  /**
   * A formatter for text fields that only allows for integers.
   */
  public static UnaryOperator<TextFormatter.Change> integerFormatter = change -> {
    String newText = change.getControlNewText();
    if (newText.matches("[0-9]*")) {
      return change;
    }
    return null;
  };


  /**
   * Get the float formatter as a TextFormatter.
   *
   * @return the float formatter.
   */
  public static TextFormatter<Change> getFloatFormatter() {
    return new TextFormatter<>(floatFormatter);
  }

  /**
   * Get the integer formatter as a TextFormatter.
   *
   * @return the integer formatter.
   */
  public static TextFormatter<Change> getIntFormatter() {
    return new TextFormatter<>(integerFormatter);
  }

  /**
   * Set up a listener for a TextField to limit its maximum size.
   *
   * @param textField the TextField to set the listener on.
   * @param maxLength the maximum number of characters allowed in the TextField.
   */
  public static void limitTextFieldSize(TextField textField, int maxLength) {
    textField.textProperty().addListener((observable, oldValue, newValue) -> {
      if (newValue.length() > maxLength) {
        textField.setText(oldValue);
      }
    });
  }

  /**
   * Check if a string can be converted to a float.
   *
   * @param line the string to check.
   * @return true if the string can be converted to a float, false otherwise.
   */
  public static boolean isNotFloat(String line) {
    try {
      Float.parseFloat(line);
      return false;
    } catch (NumberFormatException e) {
      return true;
    }
  }
}
