package edu.ntnu.stud.chaosgame.controller.game;

import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.game.ChaosCanvas;
import edu.ntnu.stud.chaosgame.model.game.ChaosGameDescription;
import edu.ntnu.stud.chaosgame.view.ChaosGameObserver;
import java.util.ArrayList;
import java.util.Random;

/**
 * Class representing the chaos game, responsible for managing its progression.
 */
public class ChaosGame {

  /**
   * The ChaosCanvas for this ChaosGame.
   */
  private final ChaosCanvas canvas;

  /**
   * The chaos game description.
   */
  private ChaosGameDescription description;

  /**
   * Observers monitoring this ChaosGame.
   */
  private ArrayList<ChaosGameObserver> observers;

  /**
   * The current point in the chaos game.
   */
  private Vector2D currentPoint;

  /**
   * The random number generator for the chaos game.
   */
  private final Random random;

  /**
   * Number of transforms.
   */
  private final int numOfTransforms;


  /**
   * Basic parameterised constructor.
   *
   * @param description the ChaosGameDescription.
   * @param canvas the ChaosCanvas.
   */
  public ChaosGame(ChaosGameDescription description, ChaosCanvas canvas) {
    this.canvas = canvas;
    this.description = description;
    this.random = new Random();
    this.numOfTransforms = description.getTransforms().size();
    this.currentPoint = new Vector2D(0, 0);
  }


  /**
   * Get the canvas of this chaos game.
   *
   * @return the canvas.
   */
  public ChaosCanvas getCanvas() {
    return this.canvas;
  }

  /**
   * Get the current point in the chaos game.
   *
   * @return the point.
   */
  public Vector2D getCurrentPoint() {
    return this.currentPoint;
  }

  /**
   * Get the description for this choas game.
   *
   * @return the description.
   */
  public ChaosGameDescription getDescription() {
    return this.description;
  }

  /**
   * Get the chaos game description.
   *
   * @param description the description to set.
   */
  public void setDescription(ChaosGameDescription description) {
    this.description = description;
  }


  /**
   * Run the chaos game for n iterations.
   *
   * @param n the number of iterations.
   */
  public void runSteps(int n) {

    // Testing the RNG
    for (int i = 0; i < n; i++) {

      int j = this.random.nextInt(this.numOfTransforms);
      Vector2D newPoint = this.description.getTransforms().get(j).transform(currentPoint);

      this.currentPoint = this.findValidPoint(newPoint);
      this.canvas.putPixel(currentPoint);
    }

  }

  /**
   * If the point is out of bounds, iterate until it is within bounds.
   * If the method fails to find a valid point within 100 iterations, it returns the original
   * point.
   *
   * @param point the starting point.
   * @return the resulting valid point within bounds, or the original point if no
   *      valid point is found.
   */
  public Vector2D findValidPoint(Vector2D point) {
    Vector2D originalPoint = point;
    int i = 0;
    if (this.canvas.isPointInCanvasRange(point)) {
      while (this.canvas.isPointInCanvasRange(point) && i < 100) {
        int j = this.random.nextInt(this.numOfTransforms);
        point = this.description.getTransforms().get(j).transform(currentPoint);
        i++;
      }
    }
    if (i >= 100) {
      return originalPoint;
    } else {
      return point;
    }
  }

}
