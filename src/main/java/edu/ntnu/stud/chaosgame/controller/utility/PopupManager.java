package edu.ntnu.stud.chaosgame.controller.utility;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * This class handles error popups across the ChaosGame application.
 */
public class PopupManager {

  /**
   * Display a basic error.
   *
   * @param header the header of the popup message.
   * @param content the content of the popup message.
   */
  public static void displayError(String header,  String content) {
    Alert alert = new Alert(AlertType.ERROR);
    alert.setHeaderText(header);
    alert.setContentText(content);
    alert.showAndWait();
  }
}
