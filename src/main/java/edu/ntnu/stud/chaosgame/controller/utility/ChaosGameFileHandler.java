package edu.ntnu.stud.chaosgame.controller.utility;

import edu.ntnu.stud.chaosgame.model.data.Complex;
import edu.ntnu.stud.chaosgame.model.data.Matrix2x2;
import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.game.ChaosGameDescription;
import edu.ntnu.stud.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.stud.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.stud.chaosgame.model.transformations.Transform2D;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Class for handling the reading and writing of chaos game files.
 */
public class ChaosGameFileHandler {

  /**
   * Creates a ChaosGameDescription based on the file found at the input path.
   *
   * @param path A String input which gives the path to a file describing a chaos game.
   * @return A {@link ChaosGameDescription} description of a chaos game.
   */
  public ChaosGameDescription readFromFile(String path) {

    // Declare variables
    Vector2D minimumVector;
    Vector2D maximumVector;
    List<Transform2D> transforms = new ArrayList<>();
    String name;

    Path paths = Paths.get(path);

    try (Scanner scanner = new Scanner(Files.newInputStream(paths))) {
      scanner.useLocale(Locale.ENGLISH);

      // Use a delimiter that splits on commas and new lines, and ignores # comments
      scanner.useDelimiter(",\\s*|\\r?\\n|(\\s*#.*\\s*)");
      String firstLine = scanner.next().trim();

      // Second line should be the name of the chaos game description.
      name = scanner.next().trim();


      if (firstLine.equals("Affine2D") && Formatter.isNotFloat(name)) {
        // Read the minimum vector
        if (scanner.hasNextDouble()) {
          double x0min = scanner.nextDouble();
          double x1min = scanner.nextDouble();
          minimumVector = new Vector2D(x0min, x1min);
        } else {
          throw new IllegalArgumentException("Expected minimum vector coordinates.");
        }

        // Read the maximum vector
        if (scanner.hasNextDouble()) {
          double x0max = scanner.nextDouble();
          double x1max = scanner.nextDouble();
          maximumVector = new Vector2D(x0max, x1max);
        } else {
          throw new IllegalArgumentException("Expected maximum vector coordinates.");
        }


        // Read the transforms
        while (scanner.hasNextDouble()) {
          double a00 = scanner.nextDouble();
          double a01 = scanner.nextDouble();
          double a10 = scanner.nextDouble();
          double a11 = scanner.nextDouble();
          double b0 = scanner.nextDouble();
          double b1 = scanner.nextDouble();

          Matrix2x2 matrix2x2 = new Matrix2x2(a00, a01, a10, a11);
          Vector2D vector2D = new Vector2D(b0, b1);
          Transform2D transform2D = new AffineTransform2D(matrix2x2, vector2D);
          transforms.add(transform2D);

        }
      } else if (firstLine.equals("Julia") && Formatter.isNotFloat(name)) {
        // Read the minimum vector
        if (scanner.hasNextDouble()) {
          double x0min = scanner.nextDouble();
          double x1min = scanner.nextDouble();
          minimumVector = new Vector2D(x0min, x1min);
        } else {
          throw new IllegalArgumentException("Expected minimum vector coordinates.");
        }

        // Read the maximum vector
        if (scanner.hasNextDouble()) {
          double x0max = scanner.nextDouble();
          double x1max = scanner.nextDouble();
          maximumVector = new Vector2D(x0max, x1max);
        } else {
          throw new IllegalArgumentException("Expected maximum vector coordinates.");
        }

        // Read the transforms
        while (scanner.hasNextDouble()) {
          double creal = scanner.nextDouble();
          double cimag = scanner.nextDouble();
          Complex complex = new Complex(creal, cimag);
          JuliaTransform juliaTransform1 = new JuliaTransform(complex, 1);
          JuliaTransform juliaTransform2 = new JuliaTransform(complex, -1);
          transforms.add(juliaTransform1);
          transforms.add(juliaTransform2);
        }

      } else if (!Formatter.isNotFloat(name)) {
        throw new IllegalStateException("Invalid Chaos Game, expected a name on line 2.");
      } else {
        throw new IllegalStateException("Invalid Chaos Game, expected 'Affine2D' or 'Julia' "
            + "on first line.");
      }
    } catch (Exception e) {
      PopupManager.displayError("Read from file error.", "Failed to read a "
          + "valid chaos game file at " + path);
      return null;
    }

    return new ChaosGameDescription(minimumVector, maximumVector, transforms, name);
  }

  /**
   * Reads from a file.
   *
   * @param description A {@link ChaosGameDescription} description
   *                    of the chaos game that should be written to file.
   * @param path A String describing the path to the file that should be written to.
   * @throws IOException if the file cannot be written to.
   */
  public void writeToFile(ChaosGameDescription description, String path) throws IOException {

    List<Transform2D> transforms = description.getTransforms();
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {

      // Check for transform type and write name
      if (transforms.getFirst() instanceof AffineTransform2D) {
        writer.write("Affine2D\n");
      } else if (transforms.getFirst() instanceof JuliaTransform) {
        writer.write("Julia\n");
      }

      writer.write(description.getName() + "\n");

      // Write minimum and maximum vectors
      Vector2D minVector2D = description.getMinCoords();
      Vector2D maxVector2D = description.getMaxCoords();
      writer.write(minVector2D.getX0() + ", " + minVector2D.getX1() + "\n");
      writer.write(maxVector2D.getX0() + ", " + maxVector2D.getX1() + "\n");

      // Write transformation values for correct transformation
      for (Transform2D transform : transforms) {
        if (transform instanceof AffineTransform2D affineTransform2D) {
          Matrix2x2 matrix2x2 = affineTransform2D.getMatrix();
          Vector2D vector2D = affineTransform2D.getVector();
          writer.write(matrix2x2.getA00() + ", " + matrix2x2.getA01() + ", "
              + matrix2x2.getA10() + ", "
              + matrix2x2.getA11() + ", " + vector2D.getX0() + ", " + vector2D.getX1() + "\n");

        } else if (transform instanceof JuliaTransform juliaTransform) {
          Complex complex = juliaTransform.getC1();
          writer.write(complex.getX0() + ", " + complex.getX1() + "\n");
        }
      }

    } catch (IOException e) {
      PopupManager.displayError("Write to file error.", "Failed to write to file");
    }
  }
}

