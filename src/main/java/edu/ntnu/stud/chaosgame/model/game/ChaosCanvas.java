package edu.ntnu.stud.chaosgame.model.game;

import edu.ntnu.stud.chaosgame.model.data.Matrix2x2;
import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.transformations.AffineTransform2D;


/**
 * Class representing the canvas for the display of the chaos game.
 */
public class ChaosCanvas {

  /**
   * Table representing the canvas.
   */
  private final int[][] canvas;

  /**
   * This array keeps track of how many times a pixel has been visited,
   * to be used to determine the color of the pixel when displayed.
   */
  private final int[][] canvasIntensityArray;

  /**
   * Width of the canvas.
   */
  private final int width;

  /**
   * Height of the canvas.
   */
  private final int height;

  /**
   * The minimum coordinates of the canvas.
   */
  private final Vector2D minCoords;

  /**
   * The maximum coordinates of the canvas.
   */
  private final Vector2D maxCoords;

  /**
   * Affine transformation for converting coordinates to canvas indices.
   */
  private final AffineTransform2D transformCoordsToIndices;

  /**
   * Parameterized constructor for the class.
   *
   * @param width width of the canvas
   * @param height height of the canvas
   * @param minCoords minimum coordinates of the canvas
   * @param maxCoords maximum coordinates of the canvas
   */
  public ChaosCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;

    // Instantiate the canvas and its intensity array.
    this.canvas = new int[width][height];
    this.canvasIntensityArray = new int[width][height];

    // Convert the coordinates to indices in the canvas
    this.transformCoordsToIndices = new AffineTransform2D(
        new Matrix2x2(0, (height - 1) / (minCoords.getX1() - maxCoords.getX1()),
        (width - 1) / (maxCoords.getX0() - minCoords.getX0()), 0),
        new Vector2D(((height - 1) * maxCoords.getX1()) / (maxCoords.getX1() - minCoords.getX1()),
        (width - 1) * minCoords.getX0() / (minCoords.getX0() - maxCoords.getX0())));
  }

  /**
   * Place a pixel on the canvas. If the pixel is out of bounds, it is ignored.
   *
   * @param point the point where the pixel is to be placed.
   */
  public void putPixel(Vector2D point) {
    if (point.getX0() > maxCoords.getX0() || point.getX0() < minCoords.getX0()
        || point.getX1() > maxCoords.getX1() || point.getX1() < minCoords.getX1()) {
      return;
    }
    int xindex = (int) transformCoordsToIndices.transform(point).getX0();
    int yindex = (int) transformCoordsToIndices.transform(point).getX1();

    if (xindex >= 0 && xindex < width && yindex >= 0 && yindex < height) {
      canvas[xindex][yindex] = 1;
      canvasIntensityArray[xindex][yindex]++;
    }
  }


  /**
   * Get the width and height of the canvas in the form of an array where the first
   * index stores the width and the second stores the height.
   *
   * @return an array of two numbers containing both the width and height.
   */
  public int[] getSize() {
    return new int[]{this.width, this.height};
  }

  /**
   * Get the vector storing the minimum coordinates.
   *
   * @return the vector.
   */
  public Vector2D getMinCoords() {
    return this.minCoords;
  }

  /**
   * Get the vector storing the maximum coordinates.
   *
   * @return the vector.
   */
  public Vector2D getMaxCoords() {
    return this.maxCoords;
  }

  /**
   * Get a pixel located at a point.
   *
   * @param point point at which the pixel is located
   * @return the pixel
   */
  public int getPixel(Vector2D point) {
    return canvas[ (int) transformCoordsToIndices.transform(point).getX0()]
        [(int) transformCoordsToIndices.transform(point).getX1()];
  }

  /**
   * Get the intensity of a pixel located at a point.
   *
   * @param point the point to check for.
   * @return the intensity at the point.
   */
  public int getIntensityPixel(Vector2D point) {
    return canvasIntensityArray[ (int) transformCoordsToIndices.transform(point).getX0()]
        [(int) transformCoordsToIndices.transform(point).getX1()];
  }

  /**
   * Get the canvas intensity array.
   *
   * @return the canvas intensity array.
   */
  public int[][] getCanvasIntensityArray() {
    return this.canvasIntensityArray;
  }

  /**
   * Clear the canvas of all content.
   */
  public void clearCanvas() {
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        canvas[i][j] = 0;
      }
    }
  }

  /**
   * Get the array defining the size of the canvas.
   *
   * @return the array.
   */
  public int[][] getCanvasArray() {
    return this.canvas;
  }

  /**
   * Checks whether a given point is within the current subsection of the coordinate space.
   *
   * @param point the point to check
   * @return true if the point is within the subsection, false otherwise
   */
  public boolean isPointInCanvasRange(Vector2D point) {
    return !(point.getX0() >= minCoords.getX0()) || !(point.getX0() <= maxCoords.getX0())
        || !(point.getX1() >= minCoords.getX1()) || !(point.getX1() <= maxCoords.getX1());
  }

  /**
   * Get the width of the ChaosCanvas.
   *
   * @return the width of the ChaosCanvas.
   */
  public int getWidth() {
    return width;
  }

  /**
   * Get the height of the ChaosCanvas.
   *
   * @return the height of the ChaosCanvas.
   */
  public int getHeight() {
    return height;
  }
}
