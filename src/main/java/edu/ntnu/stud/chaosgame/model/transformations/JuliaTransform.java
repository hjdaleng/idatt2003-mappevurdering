package edu.ntnu.stud.chaosgame.model.transformations;

import edu.ntnu.stud.chaosgame.controller.utility.ChaosGameFileHandler;
import edu.ntnu.stud.chaosgame.model.data.Complex;
import edu.ntnu.stud.chaosgame.model.data.Vector2D;

/**
 * Represents Julia transformations in a 2D-plane by extending the abstract
 * class Transform2D {@link Transform2D}.
 */
public class JuliaTransform extends Transform2D {

  /**
   * The complex number represented through Complex {@link Complex} which
   * is added or subtracted in the Julia transformations.
   */
  private final Complex c1;

  /**
   * The sign used to determine if the Julia transformations adds or subtracts {@link Complex} c1.
   */
  int sign;

  /**
   * Constructs a JuliaTransform object defined by the input.
   *
   * @param point The complex number {@link Complex}
   *              which is added or subtracted in the transformation.
   * @param sign An integer which determines if c1 is added or subtracted in the transformation.
   */
  public JuliaTransform(Complex point, int sign) {
    this.c1 = point;
    this.sign = sign;
  }

  /**
   * Performs a Julia-transformation on a point defined by the vector point.
   * The transformation will add or subtract c1 relative to point.
   * This depends on the sign of the integer sign.
   * Then  the method performs the sqrt method from Complex {@link Complex}.
   *
   * @param point The vector {@link Vector2D} which transformations are performed on.
   * @return The transformed point, represented by a vector {@link Vector2D}
   */
  public Vector2D transform(Vector2D point) {
    if (point == null) {
      throw new IllegalArgumentException("Point should not be null");
    }
    Vector2D temp1;

    if (sign > 0) {
      temp1 = point.subtract(c1);
      return new Complex(temp1.getX0(), temp1.getX1()).sqrt();
    } else if (sign < 0) {
      temp1 = point.subtract(c1);
      Complex sqrtResult = new Complex(temp1.getX0(), temp1.getX1()).sqrt();
      sqrtResult = new Complex(-sqrtResult.getX0(), -sqrtResult.getX1());
      return sqrtResult;

    } else {
      // If sign is zero, simply return the point.
      return new Complex(point.getX0(), point.getX1());
    }

  }

  /**
   * Getter method to use with {@link ChaosGameFileHandler}.
   *
   * @return The complex number used in the transformation.
   */
  public Complex getC1() {
    return this.c1;
  }
}
