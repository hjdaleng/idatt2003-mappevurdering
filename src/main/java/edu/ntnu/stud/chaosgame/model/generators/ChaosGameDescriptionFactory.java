package edu.ntnu.stud.chaosgame.model.generators;

import edu.ntnu.stud.chaosgame.controller.utility.ChaosGameFileHandler;
import edu.ntnu.stud.chaosgame.controller.utility.PopupManager;
import edu.ntnu.stud.chaosgame.model.game.ChaosGameDescription;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


/**
 * Class that generates a set number of ChaosGameDescription instances using a set of
 * files read by a ChaosGameFileHandler.
 */
public class ChaosGameDescriptionFactory {

  /**
   * The list of all the descriptions generated from the data in the directory specified
   * in constructor.
   */
  private final ArrayList<ChaosGameDescription> descriptions;

  /**
   * The file handler for reading the files.
   */
  private final ChaosGameFileHandler fileHandler;

  /**
   * Constructor for the class.
   */
  public ChaosGameDescriptionFactory() {
    this.descriptions = new ArrayList<>();
    this.fileHandler = new ChaosGameFileHandler();
    loadDescriptions();
  }

  /**
   * Load the descriptions from the directory.
   */
  private void loadDescriptions() {
    Path dir = Paths.get("src/main/resources/descriptions/saved_descriptions/");
    try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*.txt")) {
      for (Path entry : stream) {
        ChaosGameDescription description = fileHandler.readFromFile(entry.toString());
        if (description != null) {
          descriptions.add(description);
        }
      }
    } catch (IOException e) {
      PopupManager.displayError("Error", "Could not load descriptions from "
          + "directory");
    }
  }

  /**
   * Get the list of descriptions.
   *
   * @return the list.
   */
  public List<ChaosGameDescription> getDescriptions() {
    return descriptions;
  }
}
