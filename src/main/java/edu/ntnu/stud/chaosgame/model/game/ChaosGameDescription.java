package edu.ntnu.stud.chaosgame.model.game;

import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.transformations.Transform2D;
import java.util.List;

/**
 * Description of the chaos game, representing its mathematical properties.
 */
public class ChaosGameDescription {

  /**
   * The name of the chaos game.
   */
  private String name;

  /**
   * The minimum coordinates.
   */
  private final Vector2D minCoords;

  /**
   * The maximum coordinates.
   */
  private final Vector2D maxCoords;

  /**
   * The affine transforms for this chaos game description.
   */
  private final List<Transform2D> transforms;

  /**
   * Constructor for ChaosGameDescription.
   *
   * @param minCoords Inputs a {@link Vector2D}
   *                  vector of lower left coordinates for the chaos game.
   * @param maxCoords Inputs a {@link Vector2D}
   *                  vector of upper right coordinates for the chaos game.
   * @param transforms Inputs a list of transformations
   *                   {@link edu.ntnu.stud.chaosgame.model.transformations.AffineTransform2D}
   *                   used in the chaos game.
   */
  public ChaosGameDescription(Vector2D minCoords, Vector2D maxCoords,
      List<Transform2D> transforms) {
    if (minCoords == null || maxCoords == null || transforms == null) {
      throw new IllegalArgumentException("Coordinates and transforms must not be null.");
    }
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
  }

  /**
   * Parameterised constructor for ChaosGameDescription with name.
   *
   * @param minCoords Inputs a {@link Vector2D} vector of lower left coordinates for the chaos game.
   * @param maxCoords Inputs a {@link Vector2D} vector of
   *                  upper right coordinates for the chaos game.
   * @param transforms Inputs a list of transformations
   *                   {@link edu.ntnu.stud.chaosgame.model.transformations.AffineTransform2D}
   *                   used in the chaos game.
   * @param name the name of the chaos game description.
   */
  public ChaosGameDescription(Vector2D minCoords, Vector2D maxCoords, List<Transform2D> transforms,
      String name) {
    if (minCoords == null || maxCoords == null || transforms == null) {
      throw new IllegalArgumentException("Coordinates and transforms must not be null.");
    }
    this.name = name;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
  }

  /**
   * Getter method for transforms.
   *
   * @return Returns a list of transforms in the chaos game.
   */
  public List<Transform2D> getTransforms() {
    return transforms;
  }

  /**
   * Getter method for minimum coordinates.
   *
   * @return Returns a Vector2D containing the minimum coordinates.
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /**
   * Getter method for maximum coordinates.
   *
   * @return Returns a Vector2D containing the maximum coordinates.
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  /**
   * Getter method for the name of the chaos game.
   *
   * @return the name of the chaos game description.
   */
  public String getName() {
    return name;
  }
}
