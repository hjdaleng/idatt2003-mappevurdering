package edu.ntnu.stud.chaosgame.view;

import edu.ntnu.stud.chaosgame.model.game.ChaosCanvas;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * This class converts the state of a ChaosCanvas to a WritableImage.
 */
public class ChaosCanvasToImageConverter {

  /**
   * The image to be created or reused.
   */
  private WritableImage image;

  /**
   * Convert the canvas to a writable image.
   *
   * @param chaosCanvas the canvas to work upon.
   * @param useIntensity whether to use intensity for coloring.
   * @param image the reusable image.
   */
  public ChaosCanvasToImageConverter(ChaosCanvas chaosCanvas,
      boolean useIntensity, WritableImage image) {
    this.image = image;
    if (!useIntensity) {
      this.convertWithoutIntensity(chaosCanvas);
    } else {
      this.convertWithIntensity(chaosCanvas);
    }
  }

  /**
   * Get the image.
   *
   * @return the image.
   */
  public WritableImage getImage() {
    return image;
  }

  /**
   * Convert the ChaosCanvas to an image using the intensity of the pixels
   * to determine the color.
   *
   * @param canvas the ChaosCanvas to convert.
   */
  public void convertWithIntensity(ChaosCanvas canvas) {
    int width = canvas.getWidth();
    int height = canvas.getHeight();


    int[][] canvasArray = canvas.getCanvasArray();
    int[][] canvasIntensityArray = canvas.getCanvasIntensityArray();
    int maxIntensity = 0;

    // Find the maximum intensity.
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        if (canvasArray[i][j] == 1 && canvasIntensityArray[i][j] > maxIntensity) {
          maxIntensity = canvasIntensityArray[i][j];
        }
      }
    }

    // If maxIntensity is zero, we avoid division by zero.
    if (maxIntensity == 0) {
      maxIntensity = 1;
    }

    // Create a pixel buffer
    int[] pixelBuffer = new int[width * height];

    // Map the intensity to a hue value.
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        if (canvasArray[i][j] == 1) {
          int intensity = canvasIntensityArray[i][j];
          double hue = (double) intensity / maxIntensity * 360;
          Color color = Color.hsb(hue, 1.0, 1.0);
          pixelBuffer[i * width + j] = (int) (color.getOpacity() * 255) << 24
              | (int) (color.getRed() * 255) << 16
              | (int) (color.getGreen() * 255) << 8
              | (int) (color.getBlue() * 255);
        } else {
          pixelBuffer[i * width + j] = 0xFFFFFFFF;
        }
      }
    }
    PixelWriter pixelWriter = image.getPixelWriter();
    // Write the pixel buffer to the image
    pixelWriter.setPixels(0, 0, width, height,
        javafx.scene.image.PixelFormat.getIntArgbInstance(), pixelBuffer, 0, width);
  }

  /**
   * Convert the ChaosCanvas to an image without concern for the intensity of the pixels.
   *
   * @param chaosCanvas the ChaosCanvas to convert.
   */
  public void convertWithoutIntensity(ChaosCanvas chaosCanvas) {
    int width = chaosCanvas.getWidth();
    int height = chaosCanvas.getHeight();
    PixelWriter pixelWriter = image.getPixelWriter();

    int[][] canvasArray = chaosCanvas.getCanvasArray();

    int[] pixelBuffer = new int[width * height];

    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        if (canvasArray[i][j] == 1) {
          pixelBuffer[i * width + j] = 0xFF000000;
        } else {
          pixelBuffer[i * width + j] = 0xFFFFFFFF;
        }
      }
    }
    pixelWriter.setPixels(0, 0, width, height,
        javafx.scene.image.PixelFormat.getIntArgbInstance(), pixelBuffer, 0, width);
  }
}
