package edu.ntnu.stud.chaosgame.view.modificationpopups;

import edu.ntnu.stud.chaosgame.controller.utility.Formatter;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Abstract class for popups.
 */
public abstract class AbstractPopup {

  /**
   * The stage of the popup.
   */
  protected Stage popupModifyStage;

  /**
   * The layout of the popup.
   */
  protected VBox layout;

  /**
   * The scene of the popup.
   */
  protected Scene scene;

  /**
   * Common elements of the popup.
   */
  protected TextField nameTextField, minXTextField, minYTextField, maxXTextField, maxYTextField;

  /**
   * The update button.
   */
  protected Button updateButton;

  /**
   * The bottom layout of the popup.
   */
  protected VBox bottomLayout;

  /**
   * Constructor for AbstractPopup.
   *
   * @param layout the layout of the popup.
   */
  protected AbstractPopup(VBox layout) {
    this.popupModifyStage = new Stage();
    this.popupModifyStage.initModality(Modality.APPLICATION_MODAL);
    this.popupModifyStage.setAlwaysOnTop(true);
    this.layout = layout;
    this.scene = new Scene(layout);
    this.popupModifyStage.setScene(scene);
    initializeCommonElements();
  }

  /**
   * Initializes common elements of the popup.
   */
  private void initializeCommonElements() {
    nameTextField = new TextField();
    HBox nameBox = new HBox(new Label("Name: "), nameTextField);

    minXTextField = createNumericTextField();
    minYTextField = createNumericTextField();
    maxXTextField = createNumericTextField();
    maxYTextField = createNumericTextField();
    updateButton = new Button("Update");

    updateButton.setStyle("-fx-background-color: #006400;"
        + " -fx-padding: 8 16; -fx-background-radius: 16; -fx-background-insets: 1px;"
        + " -fx-border-width: 2px; -fx-border-color: white; -fx-border-radius: 16;"
        + "-fx-font-size: 14; -fx-text-fill: white;");

    minXTextField.setPrefWidth(50);
    minYTextField.setPrefWidth(50);
    maxXTextField.setPrefWidth(50);
    maxYTextField.setPrefWidth(50);

    GridPane minCoordsGrid = new GridPane();
    minCoordsGrid.setHgap(10);
    minCoordsGrid.add(new Label("Min Coordinates: "), 0, 0);
    minCoordsGrid.add(minXTextField, 1, 0);
    minCoordsGrid.add(minYTextField, 2, 0);

    GridPane maxCoordsGrid = new GridPane();
    maxCoordsGrid.setHgap(10);
    maxCoordsGrid.add(new Label("Max Coordinates: "), 0, 0);
    maxCoordsGrid.add(maxXTextField, 1, 0);
    maxCoordsGrid.add(maxYTextField, 2, 0);

    bottomLayout = new VBox(10, minCoordsGrid, maxCoordsGrid, updateButton);
    bottomLayout.setAlignment(Pos.CENTER);
    bottomLayout.setPadding(new Insets(10));

    VBox.setVgrow(bottomLayout, Priority.ALWAYS);

    layout.getChildren().addFirst(nameBox);
  }

  /**
   * Creates a numeric text field with a float formatter.
   *
   * @return the created numeric text field.
   */
  protected TextField createNumericTextField() {
    TextField numberField = new TextField();
    numberField.setTextFormatter(Formatter.getFloatFormatter());
    return numberField;
  }

  /**
   * Initializes the layout of the popup.
   *
   * @return the initialized layout.
   */
  public static VBox initializeLayout() {
    VBox layout = new VBox();
    layout.setPadding(new Insets(10));
    layout.setSpacing(10);
    return layout;
  }

  /**
   * Displays the popup.
   */
  public void display() {
    popupModifyStage.show();
  }

  /**
   * Gets the stage of the popup.
   *
   * @return the stage of the popup.
   */
  public Stage getPopupModifyStage() {
    return popupModifyStage;
  }

  /**
   * Gets the text field for the name.
   *
   * @return the text field for the name.
   */
  public TextField getNameTextField() {
    return nameTextField;
  }

  /**
   * Gets the text field for the minimum x value.
   *
   * @return the text field for the minimum x value.
   */
  public TextField getMinXTextField() {
    return minXTextField;
  }

  /**
   * Gets the text field for the minimum y value.
   *
   * @return the text field for the minimum y value.
   */
  public TextField getMinYTextField() {
    return minYTextField;
  }

  /**
   * Gets the text field for the maximum x value.
   *
   * @return the text field for the maximum x value.
   */
  public TextField getMaxXTextField() {
    return maxXTextField;
  }

  /**
   * Gets the text field for the maximum y value.
   *
   * @return the text field for the maximum y value.
   */
  public TextField getMaxYTextField() {
    return maxYTextField;
  }

  /**
   * Gets the update button.
   *
   * @return the update button.
   */
  public Button getUpdateButton() {
    return updateButton;
  }
}

