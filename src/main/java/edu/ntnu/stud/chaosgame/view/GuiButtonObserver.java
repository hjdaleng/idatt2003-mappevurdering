package edu.ntnu.stud.chaosgame.view;

/**
 * Observer interface for monitoring changes to the GUI buttons.
 */
public interface GuiButtonObserver {

  /**
   * Used when start-button is pressed.
   */
  void onStartButtonPressed();

  /**
   * Used when stop-button is pressed.
   */
  void onStopButtonPressed();

  /**
   * Used when clear-button is pressed.
   */
  void onClearButtonPressed();

  /**
   * Used when quit-button is pressed.
   */
  void onQuitButtonPressed();

  /**
   * Used when save image button is pressed.
   */
  void onSaveImageButtonPressed();

  /**
   * Used when load fractal button is pressed.
   */
  void onLoadFractalFromFileButtonPressed();

  /**
   * Used when write to file button is pressed.
   */
  void onWriteToFileButtonPressed();

  /**
   * Used when modify game button is pressed.
   */
  void onModifyGameButtonPressed();
}
