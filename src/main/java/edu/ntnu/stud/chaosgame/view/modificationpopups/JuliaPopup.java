package edu.ntnu.stud.chaosgame.view.modificationpopups;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import edu.ntnu.stud.chaosgame.model.data.Complex;
import edu.ntnu.stud.chaosgame.model.game.ChaosGameDescription;
import edu.ntnu.stud.chaosgame.model.transformations.JuliaTransform;

/**
 * Popup for modifying the Julia set.
 */
public class JuliaPopup extends AbstractPopup {

  /**
   * The text field for the real part of the complex number.
   */
  private final TextField realPartTextField;

  /**
   * The text field for the imaginary part of the complex number.
   */
  private final TextField imagPartTextField;

  /**
   * Constructor for the JuliaPopup.
   *
   * @param gameDescription the chaos game description to be modified.
   */
  public JuliaPopup(ChaosGameDescription gameDescription) {
    super(AbstractPopup.initializeLayout());

    realPartTextField = createNumericTextField();
    imagPartTextField = createNumericTextField();

    GridPane complexNumberGridPane = new GridPane();
    complexNumberGridPane.setHgap(10);
    complexNumberGridPane.setVgap(10);
    complexNumberGridPane.setPadding(new Insets(10));

    complexNumberGridPane.add(new Label("Real Part: "), 0, 0);
    complexNumberGridPane.add(realPartTextField, 1, 0);

    complexNumberGridPane.add(new Label("Imaginary Part: "), 0, 1);
    complexNumberGridPane.add(imagPartTextField, 1, 1);

    layout.getChildren().add(complexNumberGridPane);
    layout.getChildren().add(bottomLayout);
    popupModifyStage.setTitle("Julia Set Modification");
    displayCurrentValues(gameDescription);
    display();
  }

  /**
   * Displays the current values from the ChaosGameDescription.
   *
   * @param gameDescription the ChaosGameDescription to display values from.
   */
  private void displayCurrentValues(ChaosGameDescription gameDescription) {
    if (gameDescription != null) {
      nameTextField.setText(gameDescription.getName());
      minXTextField.setText(String.valueOf(gameDescription.getMinCoords().getX0()));
      minYTextField.setText(String.valueOf(gameDescription.getMinCoords().getX1()));
      maxXTextField.setText(String.valueOf(gameDescription.getMaxCoords().getX0()));
      maxYTextField.setText(String.valueOf(gameDescription.getMaxCoords().getX1()));

      JuliaTransform transform = (JuliaTransform) gameDescription.getTransforms().get(0);
      Complex complexNumber = transform.getC1();
      realPartTextField.setText(String.valueOf(complexNumber.getX0()));
      imagPartTextField.setText(String.valueOf(complexNumber.getX1()));
    }
  }

  /**
   * Gets the real part text field.
   *
   * @return the real part text field.
   */
  public TextField getRealPartTextField() {
    return realPartTextField;
  }

  /**
   * Gets the imaginary part text field.
   *
   * @return the imaginary part text field.
   */
  public TextField getImagPartTextField() {
    return imagPartTextField;
  }
}




