package edu.ntnu.stud.chaosgame.view;

import edu.ntnu.stud.chaosgame.model.game.ChaosGameDescription;

/**
 * Observer interface for monitoring changes to the popup.
 */
public interface PopupObserver {

  /**
   * Update the observer based on changes to the chaos game description.
   *
   * @param description the description this observer is monitoring.
   */
  void onUpdate(ChaosGameDescription description);
}