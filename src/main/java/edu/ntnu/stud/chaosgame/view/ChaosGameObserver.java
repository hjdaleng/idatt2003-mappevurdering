package edu.ntnu.stud.chaosgame.view;

import edu.ntnu.stud.chaosgame.controller.game.ChaosGame;
import edu.ntnu.stud.chaosgame.model.game.ChaosCanvas;

/**
 * Observer interface for monitoring changes to the active.
 */
public interface ChaosGameObserver {

  /**
   * Update the ChaosCanvas.
   *
   * @param canvas the canvas to update with.
   */
  void updateCanvas(ChaosCanvas canvas);

  /**
   * Update the observer based on changes to the chaos game.
   *
   * @param game the game this observer is monitoring.
   */
  void updateGame(ChaosGame game);
}
