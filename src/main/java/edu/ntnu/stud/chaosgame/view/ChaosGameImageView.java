package edu.ntnu.stud.chaosgame.view;

import edu.ntnu.stud.chaosgame.controller.utility.PopupManager;
import javafx.scene.image.ImageView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.transform.Affine;

/**
 * This class extends ImageView to implement proper zooming and panning according to the
 * requirements of the chaos game.
 */
public class ChaosGameImageView extends ImageView {

  /**
   * The controller for this class: a chaos game GUI view.
   */
  private final ChaosGameGui controller;

  /**
   * Affine initialised to the identity matrix, representing the current transformation as a result
   * of zooming.
   */
  private final Affine transform = new Affine();

  /**
   * The starting position of the mouse cursor when dragging.
   */
  private double startX;

  /**
   * The starting position of the mouse cursor when dragging.
   */
  private double startY;

  /**
   * The factor representing the magnitude of the current zoom.
   */
  private double zoomFactor = 1.0;

  /**
   * Integer representing how many recursive levels of QuadTrees are required to get to the current
   * zoom magnitude.
   */
  private int zoomLevel = 0;

  /**
   * Constructor for the ChaosGameImageView.
   *
   * @param controller the GUI which controls this image view.
   */
  public ChaosGameImageView(ChaosGameGui controller) {
    this.setOnScroll(this::affineZoom);
    this.setOnMousePressed(this::mousePressed);
    this.setOnMouseDragged(this::mouseDragged);
    this.getTransforms().add(transform);
    this.controller = controller;
  }

  /**
   * Zooms the image view in or out based on the scroll event.
   *
   * @param event the event.
   */
  private synchronized void affineZoom(ScrollEvent event) {
    double newZoomFactor = event.getDeltaY() > 0 ? 1.05 : 1 / 1.05;
    try {
      // Get the old values
      double oldScaleX = transform.getMxx();
      double oldScaleY = transform.getMyy();
      double oldTranslateX = transform.getTx();
      double oldTranslateY = transform.getTy();

      // Compute the new values
      double newScaleX = oldScaleX * newZoomFactor;
      double newScaleY = oldScaleY * newZoomFactor;
      double newTranslateX = oldTranslateX - (event.getX() * (newScaleX - oldScaleX));
      double newTranslateY = oldTranslateY - (event.getY() * (newScaleY - oldScaleY));

      // Update the transform
      transform.setMxx(newScaleX);
      transform.setMyy(newScaleY);
      transform.setTx(newTranslateX);
      transform.setTy(newTranslateY);

      this.zoomFactor *= newZoomFactor;
      this.zoomLevel = (int) (Math.log(this.zoomFactor) / Math.log(4)); // Update zoom level.

    } catch (Exception e) {
      PopupManager.displayError("Zoom error", e.getMessage());
    }
  }

  /**
   * Gets mouse cursor position data when mouse button is pressed.
   *
   * @param event the mouse event.
   */
  private synchronized void mousePressed(javafx.scene.input.MouseEvent event) {
    startX = event.getX();
    startY = event.getY();
  }

  /**
   * Drags the image view based on the mouse cursor position.
   *
   * @param event the mouse event.
   */
  private synchronized void mouseDragged(javafx.scene.input.MouseEvent event) {
    double deltaX = event.getX() - startX;
    double deltaY = event.getY() - startY;

    transform.setTx(transform.getTx() + deltaX);
    transform.setTy(transform.getTy() + deltaY);
  }

  /**
   * Gets the current transform.
   *
   * @return the current transform.
   */
  public Affine getTransform() {
    return this.transform;
  }
}
