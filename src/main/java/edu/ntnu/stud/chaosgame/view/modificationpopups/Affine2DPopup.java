package edu.ntnu.stud.chaosgame.view.modificationpopups;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import edu.ntnu.stud.chaosgame.model.data.Matrix2x2;
import edu.ntnu.stud.chaosgame.model.data.Vector2D;
import edu.ntnu.stud.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.stud.chaosgame.model.game.ChaosGameDescription;

/**
 * Popup for modifying the Affine2D transformations.
 */
public class Affine2DPopup extends AbstractPopup {

  /**
   * The matrix text fields.
   */
  private final TextField[][] matrixTextFields;

  /**
   * The vector text fields.
   */
  private final TextField[] vectorTextFields;

  /**
   * Constructor for the Affine2DPopup.
   *
   * @param gameDescription the chaos game description to be modified.
   */
  public Affine2DPopup(ChaosGameDescription gameDescription) {
    super(AbstractPopup.initializeLayout());

    int transformCount = Math.min(gameDescription.getTransforms().size(), 4);
    matrixTextFields = new TextField[transformCount][4];
    vectorTextFields = new TextField[transformCount * 2];

    for (int i = 0; i < transformCount; i++) {
      for (int j = 0; j < 4; j++) {
        matrixTextFields[i][j] = createNumericTextField();
        matrixTextFields[i][j].setPrefWidth(50);
      }
      vectorTextFields[2 * i] = createNumericTextField();
      vectorTextFields[2 * i].setPrefWidth(50);
      vectorTextFields[2 * i + 1] = createNumericTextField();
      vectorTextFields[2 * i + 1].setPrefWidth(50);
    }

    VBox matrixVBox = new VBox(10);
    for (int i = 0; i < transformCount; i++) {
      GridPane matrixGridPane = new GridPane();
      matrixGridPane.setHgap(10);
      matrixGridPane.setVgap(10);
      matrixGridPane.setPadding(new Insets(10));

      matrixGridPane.add(new Label("Matrix " + (i + 1) + " Row 1: "), 0, 0);
      matrixGridPane.add(matrixTextFields[i][0], 1, 0);
      matrixGridPane.add(matrixTextFields[i][1], 2, 0);

      matrixGridPane.add(new Label("Matrix " + (i + 1) + " Row 2: "), 0, 1);
      matrixGridPane.add(matrixTextFields[i][2], 1, 1);
      matrixGridPane.add(matrixTextFields[i][3], 2, 1);

      matrixGridPane.add(new Label("Vector " + (i + 1) + ": "), 0, 2);
      matrixGridPane.add(vectorTextFields[2 * i], 1, 2);
      matrixGridPane.add(vectorTextFields[2 * i + 1], 2, 2);

      matrixVBox.getChildren().add(matrixGridPane);
    }

    layout.getChildren().addAll(matrixVBox);
    layout.getChildren().add(bottomLayout);
    popupModifyStage.setTitle("Affine2D Modification");
    displayCurrentValues(gameDescription);
    display();
  }

  /**
   * Displays the current values from the ChaosGameDescription in the text fields.
   *
   * @param gameDescription the ChaosGameDescription to display values from.
   */
  private void displayCurrentValues(ChaosGameDescription gameDescription) {
    if (gameDescription != null) {
      nameTextField.setText(gameDescription.getName());
      minXTextField.setText(String.valueOf(gameDescription.getMinCoords().getX0()));
      minYTextField.setText(String.valueOf(gameDescription.getMinCoords().getX1()));
      maxXTextField.setText(String.valueOf(gameDescription.getMaxCoords().getX0()));
      maxYTextField.setText(String.valueOf(gameDescription.getMaxCoords().getX1()));

      int transformCount = Math.min(gameDescription.getTransforms().size(), 4);
      for (int i = 0; i < transformCount; i++) {
        AffineTransform2D transform = (AffineTransform2D) gameDescription.getTransforms().get(i);
        Matrix2x2 matrix = transform.getMatrix();
        Vector2D vector = transform.getVector();
        matrixTextFields[i][0].setText(String.valueOf(matrix.getA00()));
        matrixTextFields[i][1].setText(String.valueOf(matrix.getA01()));
        matrixTextFields[i][2].setText(String.valueOf(matrix.getA10()));
        matrixTextFields[i][3].setText(String.valueOf(matrix.getA11()));
        vectorTextFields[2 * i].setText(String.valueOf(vector.getX0()));
        vectorTextFields[2 * i + 1].setText(String.valueOf(vector.getX1()));
      }
    }
  }

  /**
   * Gets the matrix text fields.
   *
   * @return the matrix text fields.
   */
  public TextField[][] getMatrixTextFields() {
    return matrixTextFields;
  }

  /**
   * Gets the vector text fields.
   *
   * @return the vector text fields.
   */
  public TextField[] getVectorTextFields() {
    return vectorTextFields;
  }
}
