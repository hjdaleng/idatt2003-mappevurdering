package edu.ntnu.stud.chaosgame;

import edu.ntnu.stud.chaosgame.view.ChaosGameGui;
import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Main class for the Chaos Game application.
 */
public class Main extends Application {

  @Override
  public void start(Stage primaryStage) throws IOException {
    new ChaosGameGui(primaryStage);
  }

  /**
   * Main method for the Chaos Game application.
   *
   * @param args command line arguments
   */
  public static void main(String[] args) {
    launch(args);
  }
}