# Fractal Chaos Game v1.0

## Description
This application is an implementation of a fractal chaos game. 
It makes use of fractals defined via affine transformations and Julia transformations, 
generated iteratively. 
The application displays these fractals in a window and gives the user the ability to edit
which fractal is being run by either changing the transformations or editing the existing
ones. The application provides basic zoom-and panning functionality, and a simple sidebar
for user control. 

## Installation
To install the application, run the following command from your desired folder:
```
git clone https://gitlab.stud.idi.ntnu.no/hjdaleng/idatt2003-mappevurdering.git
```

The program requires that the user has Maven installed. Find more information about this here: https://maven.apache.org/install.html

## Usage
To run the application, run the following commands from the Git repository root folder:
```
mvn javafx:run
```

The user interface will then open on your monitor. There are two main components: the image view
displaying the fractal as it generates on the left, and the sidebar on the right used to control
the program. The sidebar is divided into a number of sections: *Play control, Chaos game selection, 
Color control, Step count, Game modification* and *Menu controls*. 

### Play control
Here you control the progression of the fractal generation. You may start and pause the chaos game,
or start a new one by pressing the *New* button.

### Chaos Game Selection
This is where you select which chaos game description you want to use, determining
what the fractal will look like. Note that the ComboBox only displays the descriptions found in
the folder under src/main/resources/saved_descriptions. If you wish to add your own fractals via .txt-files,
you must follow the format of existing chaos game descriptions exactly, with the type
of chaos game on the first line, and the name on the second, followed by the parameters
separated by commas.

### Color Control
By checking the _Show Redrawn Pixels_-checkbox, the application will not only show which points
have been visited throughout the chaos game's runtime, but will also indicate the number of times
a given point has been visited via a heatmap.

### Step Count
Here you may control the step count, determining how quickly the fractal is generated on screen.
Note that at most six digits can be used due to performance constraints.

### Game Modification
By pressing _Create Modified Game_, you can edit the currently selected chaos game description
by either renaming it or changing its parameters. This will alter the mathematical and geometric properties of
the fractal. However, please note that extreme values may result in the application crashing under
certain circumstances.

### Menu Controls
In the menu controls, you can save an image of the fractal as it currently appears on screen,
load an existing chaos game description from your hard drive, write the current description to
a .txt-file, and quit the application.

**Enjoy the chaos!**